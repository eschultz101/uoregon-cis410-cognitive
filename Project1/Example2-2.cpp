// Eric Schultz
// CIS 410 Project 1
// 4/14/15

#include <iostream>
#include <cstdlib>
#include <ctime>

int main(int argc, char* argv[]) {

	int r = 0, sum = 0, serviceTime = 0, tcount = 0, scount = 0, tencount = 0;
	float avg = 0.0;
	int a[10000] = { 0 };
	srand(time(NULL));

	for(int i = 0; i < 10000; ++i) {
		r = rand() % 100 + 1;
		if(r >= 1 && r <= 30) {
			a[i] = 3;
			++tcount;
		}
		if(r >= 31 && r <= 75) {
			a[i] = 6;
			++scount;	
		}  
		if(r >= 76 && r <= 100) {
			a[i] = 10;
			++tencount;	
		}
		std::cout << a[i] << "\n";
		sum += a[i];
	}
	avg = (float)sum / 10000.0;

	std::cout << "3: " << tcount << ", 6: " << scount << ", 10: " << tencount;
	std::cout << ", mean: " << avg << "\n";

	return 0;
}
