// Eric Schultz
// CIS 410 Project 1
// 4/14/15

#include <iostream>
#include <cstdlib>
#include <ctime>

int main(int argc, char* argv[]) {

	int r = 0, sum = 0, interTime = 0, count1 = 0, count2 = 0, count3 = 0, count4 = 0;
	float avg = 0.0;
	int a[10000] = { 0 };
	srand(time(NULL));

	for(int i = 0; i < 10000; ++i) {
		r = rand() % 100 + 1;
		if(r >= 1 && r <= 25) {
			a[i] = 1;
			++count1;
		}
		if(r >= 26 && r <= 50) {
			a[i] = 2;
			++count2;	
		}
		if(r >= 51 && r <= 75) {
			a[i] = 3;
			++count3;	
		}  
		if(r >= 76 && r <= 100) {
			a[i] = 4;
			++count4;	
		}
		std::cout << a[i] << "\n";
		sum += a[i];
	}
	avg = (float)sum / 10000.0;

	std::cout << "1: " << count1 << ", 2: " << count2 << ", 3: " << count3 << ", 4: " << count4;
	std::cout << ", mean: " << avg << "\n";

	return 0;
}
