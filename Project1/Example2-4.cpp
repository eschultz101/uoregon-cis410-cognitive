// Eric Schultz
// CIS 410 Project 1
// 4/14/15

#include <iostream>
#include <cstdlib>
#include <ctime>

int main(int argc, char* argv[]) {

	int rNum = 0, h = 0, t = 0, tpercent = 0, hpercent = 0, epercent = 0;
	int h1_10 = 0, h45_55 = 0, h90_100 = 0;
	srand(time(NULL));

	for(int i = 0; i < 1000; ++i) {
		for(int j = 0; j < 100; ++j) {
			rNum = rand() % 2;
			
			if(rNum == 0) h++;
			else t++;

			if(t > h) tpercent++;
			else if(h > t) hpercent++;
			else epercent++;
		}

		std::cout << "Tom: " << t << ", percentage of tosses ahead: " << tpercent << "\n";
		std::cout << "Harry: " << h << ", percentage of tosses ahead: " << hpercent << "\n";
		std::cout << "Percentage of tosses spent equal: " << epercent << "\n\n";
		if(hpercent <= 10) h1_10++;
		if(hpercent >= 45 && hpercent <= 55) h45_55++;
		if(hpercent >= 90) h90_100++;
		t = 0; h = 0; tpercent = 0; hpercent = 0, epercent = 0;
	}

	std::cout << "\nNumber of trials Harry is ahead for 1-10 tosses: " << h1_10;
	std::cout << "\nNumber of trials Harry is ahead for 45-55 tosses: " << h45_55;
	std::cout << "\nNumber of trials Harry is ahead for 90-100 tosses: " << h90_100;

	return 0;
}
